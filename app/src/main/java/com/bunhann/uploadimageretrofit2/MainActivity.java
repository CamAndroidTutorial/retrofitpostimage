package com.bunhann.uploadimageretrofit2;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.master.permissionhelper.PermissionHelper;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final int PICK_FROM_GALLARY = 1;
    private Button btnBrowseImage, btnUpload;
    private ImageView imageView;
    private TextView tvResponse;
    private String imagePath = null;
    private APIInterface apiInterface;
    private String jsonAuth = "{\"auth\":{\"api_key\": \"14af15ef78b4036cf3b2fe4a873952fa\", \"api_secret\": \"5844ef8e856ea48fb175b0ccab453aefba3af048\"}, \"wait\":true}";
    private PermissionHelper permissionHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnBrowseImage= (Button) findViewById(R.id.btnBrowseImage);
        imageView = (ImageView) findViewById(R.id.imageView);
        btnUpload = (Button) findViewById(R.id.btnUpload);
        tvResponse = (TextView) findViewById(R.id.tvResponse);

        permissionHelper = new PermissionHelper(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.INTERNET}, 100);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        btnBrowseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                permissionHelper.request(new PermissionHelper.PermissionCallback() {
                    @Override
                    public void onPermissionGranted() {
                        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        // Start the Intent
                        startActivityForResult(galleryIntent, PICK_FROM_GALLARY);
                    }

                    @Override
                    public void onIndividualPermissionGranted(@NotNull String[] strings) {
                        if (strings[0]==Manifest.permission.READ_EXTERNAL_STORAGE) {
                            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            // Start the Intent
                            startActivityForResult(galleryIntent, PICK_FROM_GALLARY);
                        }
                    }

                    @Override
                    public void onPermissionDenied() {
                        Toast.makeText(MainActivity.this, "Permission Need!!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onPermissionDeniedBySystem() {
                        Toast.makeText(MainActivity.this, "Permission Need!! It was deny by system!", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File file = new File(imagePath);

                RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part body = MultipartBody.Part.createFormData("upload", file.getName(), reqFile);

                RequestBody bodyAuth = RequestBody.create(MediaType.parse("application/json"), jsonAuth);
                Call<ResponseBody> uploadImage = apiInterface.postImage(bodyAuth, body);
                uploadImage.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            tvResponse.setText("Successfully Uploaded!");
                            Toast.makeText(MainActivity.this, "Done! Uploaded... You can see the link in the log...", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("Error", t.getMessage());
                    }
                });
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PICK_FROM_GALLARY:
                if (resultCode == Activity.RESULT_OK) {
                    //pick image from gallery
                    Uri selectedImage = data.getData();
                    imagePath = getPathFromURI(selectedImage);
                    imageView.setImageBitmap(BitmapFactory.decodeFile(imagePath));
                    Toast.makeText(this, imagePath, Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] filePathColumn = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(contentUri, filePathColumn, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(filePathColumn[0]);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (permissionHelper != null) {
            permissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
